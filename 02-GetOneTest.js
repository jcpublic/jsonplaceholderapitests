// 1. test that response code = 200 -- see slide #9
let actualStatusCode = responseCode["code"]
tests["Does status code == 200?"] = actualStatusCode === 200


// 2. test that a single dictionary has these keys: -- slide #11
// userId
// id
// title
// body

// 2a. Convert the response to a dictionary
let album = JSON.parse(responseBody)

// 2b. Check if the albumn has the userId key
let containsUserIdKey = "userId" in album
tests["Album has a userId key"] = containsUserIdKey === true

// 2c. Check if the album has the id key
tests["Album has a id key"] = "id" in album

// 2d. Check if the album has the title key
tests["Album has a title key"] = "title" in album

// 2e. Check if the album has the body key
tests["Album has a body key"] = "body" in album
