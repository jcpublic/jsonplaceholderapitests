// 1. check that status code = 201
tests["Status code = 201"] = responseCode["code"] === 201

// 2. check that the response has id = 101
// id = 101 is the id of the newly created album
let dataFromServer = JSON.parse(responseBody)
tests["Response contains id key"] = "id" in dataFromServer

let idOfNewAlbum = dataFromServer["id"]
tests["Id = newly created id"] = idOfNewAlbum === 101
