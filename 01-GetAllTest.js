// 1. test that response code = 200 -- see slide #9
let actualStatusCode = responseCode["code"]
tests["Does status code == 200?"] = actualStatusCode === 200

// 2. test that the response data has 100 items  --  slide #10
// 2a. convert the response data to a JSON Object type
let dataFromServer = JSON.parse(responseBody)
// 2b. get the number of items in the object
let actualNumItems = dataFromServer.length
tests["Response contains 100 items"] = actualNumItems === 100

// 3. test that a single dictionary has these keys: -- slide #11
// userId
// id
// title

// 3a. Get one item of the respone
let album = dataFromServer[0]

// 3b. Check if the albumn has the userId key
let containsUserIdKey = "userId" in album
tests["Album has a userId key"] = containsUserIdKey === true

// 3c. Check if the albumn has the id key
tests["Album has a id key"] = "id" in album

// 3d. Check if the albumn has the title key
tests["Album has a title key"] = "title" in album
